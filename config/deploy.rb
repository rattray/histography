set :application, "criticode.com"

set :scm, :git
set :repository, "git@bitbucket.org:rattray/histography.git"
set :branch, "master"
set :use_sudo, true
set :ssh_options, { :forward_agent => true }
set :deploy_via, :remote_cache

role :web, "us-il-01.criticode.net", "us-va-01.criticode.net"

set :deploy_to, "/var/www/histography.org/beta"
set :current_path, "#{deploy_to}/current"
set :releases_path, "#{deploy_to}/releases"

namespace :deploy do
	task :start do
	end
	task :stop do
	end
	task :finalize_update do
	end
	task :restart do
		run "#{deploy_to}/current/restart.sh"
		run "#{try_sudo} service nginx restart"
	end
end
