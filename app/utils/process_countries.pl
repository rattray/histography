#!/usr/bin/perl

use perl5i::2;
use Text::CSV;

sub main {
	my $csv = Text::CSV->new()
		or die "Cannot use CSV: ".Text::CSV->error_diag ();

	my $newcsv = Text::CSV->new();

	open my $fh, "<:encoding(latin1)", $ARGV[0];

	while ( my $row = $csv->getline( $fh ) ) {
		my $two_char = substr($row->[0], 0, 2);
		my $slug = $row->[1];
		$slug =~ s/[^A-Za-z-]/_/g;
		$slug = lc $slug;
		my $status = $newcsv->combine(
			@{[$row->[0], $two_char, $row->[1], $row->[2], $slug]}
		);
		say scalar($newcsv->string());
	}
	$csv->eof or $csv->error_diag();
	close $fh;	
}

main() if $0 eq __FILE__;

