#!/usr/bin/perl

use perl5i::2;
use Text::CSV;

sub main {
	my $csv = Text::CSV->new({binary => 1})
		or die "Cannot use CSV: ".Text::CSV->error_diag ();

	my $newcsv = Text::CSV->new();

	open my $fh, "<:encoding(latin1)", $ARGV[0];

	while ( my $row = $csv->getline( $fh ) ) {

		my $status = $newcsv->combine(
			@{[$row->[0], $row->[1], $row->[9], 'World Bank World Development Indicators', $row->[2]]}
		);

		say scalar($newcsv->string());
	}
	$csv->eof or $csv->error_diag();
	close $fh;	
}

main() if $0 eq __FILE__;

