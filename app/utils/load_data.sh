#!/bin/bash

case "$2" in
	countries)
		echo "Loading countries"
		echo -e "================="
		time curl ${1}/datadump/countries
        ;;
	indicators)
		echo -e "\nLoading indicators"
		echo -e "=================="
		time curl ${1}/datadump/indicators
	;;
	datapoints)
		echo -e "\nLoading datapoints"
		echo -e "=================="
		for f in $(find front-end/media/data/datapoints -name "*.csv" | sort); do
			datapoint=$(basename "$f" .csv);
			echo -e "${datapoint}"
			time curl "${1}/datadump/${datapoint}"
		done
	;;
	events)
		echo -e "\nLoading events"
		echo -e "=================="
		for f in $(find front-end/media/data/events -name "*.csv" | sort); do
			event=$(basename "$f" .csv);
			echo -e "${event}"
			time curl "${1}/datadump/${event}"
		done
	;;
	*)
		echo "Usage: $0 http://some.root.url {countries|datapoints|datapoints}"
		exit 1
esac

exit 0

