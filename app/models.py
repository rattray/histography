from django.db import models
from django.contrib.auth.models import User
import datetime
from django.db.models.signals import post_save
from django.core.files import File
import os.path
from django.template.defaultfilters import slugify
from time import strftime

#check the docs for examples. 
#there are a few weirdnesses about using django-appengine, 
#especially handling dynamic images/files. We do that in the docs.

class UserProfile(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    user = models.OneToOneField(User, unique=True)
    signup_date = models.DateField(default=datetime.date.today())

    def __unicode__(self):
        return self.name
    
class Country(models.Model):
    three_code = models.CharField(max_length=3)
    two_code = models.CharField(max_length=2)
    short_name = models.CharField(max_length=100)
    long_name = models.CharField(max_length=500)
    slug = models.SlugField()

    def __unicode__(self):
        return self.short_name

class Category(models.Model):
    name = models.CharField(max_length=500)
    description = models.TextField()

    def __unicode__(self):
        return self.name

class Indicator(models.Model):
    code = models.CharField(max_length=20)
    name = models.CharField(max_length=100)
    category = models.ForeignKey(Category, related_name="indicators")
    source = models.CharField(max_length=100, default="World Bank World Development Indicators")
    description = models.TextField()

    def __unicode__(self):
        return self.name
    
class Datapoint(models.Model):
    year = models.DateField()
    country = models.ForeignKey(Country, related_name="datapoints")
    indicator = models.ForeignKey(Indicator, related_name="datapoints")
    data = models.FloatField()

    def __unicode__(self):
        return "%s : %s : %s" % (self.year.strftime("%Y"), self.country, self.indicator)

class Event(models.Model):
    country = models.ForeignKey(Country)
    category = models.ForeignKey(Category, related_name="events", null=True)
    text = models.TextField()
    author = models.ForeignKey(UserProfile, related_name="events", null=True)
    citation = models.TextField()
    date = models.DateField()
    upvotes = models.IntegerField(null=True)
    downvotes = models.IntegerField(null=True)

    @property
    def score(self):
        return self.upvotes.count() - self.downvotes.count()

    def __unicode__(self):
        return "%s (%s in %s)" % (self.text, self.country, self.date.strftime("%Y"))

class Article(models.Model):
    title = models.CharField(max_length=100)
    blurb = models.TextField()
    text = models.TextField()
    author = models.CharField(max_length=100)
    date = models.DateField()
    event = models.ForeignKey(Event, related_name="articles")
    added_by = models.ForeignKey(UserProfile)
    addded_on = models.DateField(default=datetime.date.today())

    def __unicode__(self):
        return self.title

class Comment(models.Model):
    text = models.TextField()
    author = models.ForeignKey(UserProfile, related_name="comments")
    event = models.ForeignKey(Event, related_name="comments")
    date = models.DateField(default=datetime.datetime.now())

    def __unicode__(self):
        return self.text
