from django.conf.urls.defaults import *
from views import *
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    #important general shit
    (r'^admin/', include(admin.site.urls)),
    (r'media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    #standard pages
    (r'^$', index),
    (r'^datadump/(.+)$', datadump),
    (r'^compare/$', compare),
    (r'^api/$', endpoints),
    (r'^api/data/(\w+)/(.+)/$', graph_data),
    (r'^api/events/(\w+)/$', events),
    (r'^api/indicators/$', indicators),
    (r'^api/indicator/(.+)/$', indicator_lookup),
    (r'^(\w+)/$', country)
    )
