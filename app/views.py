from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
import urllib2 as urllib

from app.models import *

import os, sys, datetime, copy, logging, settings, csv, re, json, time, ast

# Change everything in this class! Makes things pretty fast and easy
# so the basic info about the site is ubiquitous.
class globvars():
  proj_name = "histography" # change this!
  founders = [
    {'name':'Alex Rattray', #obviously, not you.
       'email':'rattray@wharton.upenn.edu',
       'url':'http://alexrattray.com',
       'blurb':'I\'m Alex. I like webdev and most things Seattle.',
       'picture':'http://a3.sphotos.ak.fbcdn.net/hphotos-ak-ash4/294486_10150342799849221_515004220_8196907_293093247_n.jpg'}, #to self-host, put images in /front-end/media/images
    {'name':'Darian Patrick',
       'email':'darianp@sas.upenn.edu',
       'url':'http://darianpatrick.com',
       'blurb':'I don\'t know what to put. I like to hack stuff.',
       'picture':'http://darianpatrick.com/jpg/me.jpg'},
    {'name':'J.J. Fliegelman',
       'email':'jj@ebnj.net',
       'url':'',
       'blurb':'Data monkey!',
       'picture':'https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-ash4/291837_2039843279322_1341480103_32062210_1765433968_n.jpg'},
    ]
  proj_description = "A graph of historical events and quantitative indicators for every country on earth."
  countries = [c for c in Country.objects.all()]
  context = {
      'countries':countries,
      'proj_name': proj_name,
      'founders': founders,
      'proj_description': proj_description,
      }

def index(request):
  gv = globvars
  context = {
    'thispage':'Home'
      }
  context = dict(context, **gv.context) #combines the 'local' context with the 'global' context
  return render_to_response('index.html', context)


def endpoints(request):
  ''' Just so /api/ does something '''
  return HttpResponse(json.dumps({
    'indicators_list': 'http://histography.org/api/indicators/',
    'indicator_detail': 'http://histography.org/api/indicator/{indicator_full_name}/',
    'historical_events': 'http://histography.org/api/events/{country_slug}/',
    'data_points': 'http://histography.org/api/data/{country_slug}/{indicator code}/',
    'country_slugs': [
      'aruba', 'andorra', 'afghanistan', 'angola', 'albania', 'arab_league_states', 'united_arab_emirates', 'argentina', 'armenia', 'american_samoa', 'antigua_and_barbuda', 'australia', 'austria', 'azerbaijan', 'burundi', 'belgium', 'benin', 'burkina_faso', 'bangladesh', 'bulgaria', 'bahrain', 'bahamas__the', 'bosnia_and_herzegovina', 'belarus', 'belize', 'bermuda', 'bolivia', 'brazil', 'barbados', 'brunei_darussalam', 'bhutan', 'botswana', 'central_african_republic', 'canada', 'switzerland', 'channel_islands', 'chile', 'china', 'c_te_d_ivoire', 'cameroon', 'congo__rep_', 'colombia', 'comoros', 'cape_verde', 'costa_rica', 'cuba', 'curacao', 'cayman_islands', 'cyprus', 'czech_republic', 'germany', 'djibouti', 'dominica', 'denmark', 'dominican_republic', 'algeria', 'east_asia___pacific', 'east_asia___pacific__all_income_levels_', 'europe___central_asia', 'europe___central_asia__all_income_levels_', 'ecuador', 'egypt__arab_rep_', 'euro_area', 'eritrea', 'spain', 'estonia', 'ethiopia', 'european_union', 'finland', 'fiji', 'france', 'faeroe_islands', 'micronesia__fed__sts_', 'gabon', 'united_kingdom', 'georgia', 'ghana', 'gibraltar', 'guinea', 'gambia__the', 'guinea-bissau', 'equatorial_guinea', 'greece', 'grenada', 'greenland', 'guatemala', 'guam', 'guyana', 'high_income', 'hong_kong_sar__china', 'honduras', 'heavily_indebted_poor_countries__hipc_', 'croatia', 'haiti', 'hungary', 'indonesia', 'isle_of_man', 'india', 'ireland', 'iran__islamic_rep_', 'iraq', 'iceland', 'israel', 'italy', 'jamaica', 'jordan', 'japan', 'kazakhstan', 'kenya', 'kyrgyz_republic', 'cambodia', 'kiribati', 'st__kitts_and_nevis', 'korea__rep_', 'kosovo', 'kuwait', 'latin_america___caribbean', 'lao_pdr', 'lebanon', 'liberia', 'libya', 'st__lucia', 'latin_america___caribbean__all_income_levels_', 'least_developed_countries__un_classification', 'low_income', 'liechtenstein', 'sri_lanka', 'lower_middle_income', 'low___middle_income', 'lesotho', 'lithuania', 'luxembourg', 'latvia', 'macao_sar__china', 'st__martin__french_part_', 'morocco', 'monaco', 'moldova', 'madagascar', 'maldives', 'middle_east___north_africa__all_income_levels_', 'mexico', 'marshall_islands', 'middle_income', 'macedonia__fyr', 'mali', 'malta', 'myanmar', 'middle_east___north_africa', 'montenegro', 'mongolia', 'northern_mariana_islands', 'mozambique', 'mauritania', 'mauritius', 'malawi', 'malaysia', 'mayotte', 'north_america', 'namibia', 'new_caledonia', 'niger', 'nigeria', 'nicaragua', 'netherlands', 'high_income__nonoecd', 'norway', 'nepal', 'new_zealand', 'high_income__oecd', 'oecd_members', 'oman', 'pakistan', 'panama', 'peru', 'philippines', 'palau', 'papua_new_guinea', 'poland', 'puerto_rico', 'korea__dem__rep_', 'portugal', 'paraguay', 'french_polynesia', 'qatar', 'romania', 'russian_federation', 'rwanda', 'south_asia', 'saudi_arabia', 'sudan', 'senegal', 'singapore', 'solomon_islands', 'sierra_leone', 'el_salvador', 'san_marino', 'somalia', 'serbia', 'sub-saharan_africa', 'south_sudan', 'sub-saharan_africa__all_income_levels_', 's_o_tom__and_principe', 'suriname', 'slovak_republic', 'slovenia', 'sweden', 'swaziland', 'sint_maarten__dutch_part_', 'seychelles', 'syrian_arab_republic', 'turks_and_caicos_islands', 'chad', 'togo', 'thailand', 'tajikistan', 'turkmenistan', 'timor-leste', 'tonga', 'trinidad_and_tobago', 'tunisia', 'turkey', 'tuvalu', 'tanzania', 'uganda', 'ukraine', 'upper_middle_income', 'uruguay', 'united_states', 'uzbekistan', 'st__vincent_and_the_grenadines', 'venezuela__rb', 'virgin_islands__u_s__', 'vietnam', 'vanuatu', 'west_bank_and_gaza', 'world', 'samoa', 'yemen__rep_', 'south_africa', 'congo__dem__rep_', 'zambia', 'zimbabwe'
    ]
  }))

def flag(country):
  f = urllib.urlopen(r'http://duckduckgo.com/?o=json&q=%s+flag' % country)
  text = f.read()
  it = json.loads(text)
  if it['Image'] != '':
    return it['Image']
  else:
    return None

def country(request, country):
  gv = globvars

  # TODO verify that validation is being handled appropriately
  country = Country.objects.get(slug=country)

  categories = [c for c in Category.objects.all()]

  context = {
	  #'flag': flag(country.short_name),
    'categories': categories,
    'country': country,
    'thispage':country.short_name
      }
  context = dict(context, **gv.context) #combines the 'local' context with the 'global' context
  return render_to_response('country.html', context)

def graph_data(request, country, indicator):
  country = Country.objects.get(slug=country)
  #indicator = Indicator.objects.get(code=indicator)
  datapoints = []
  #for d in Datapoint.objects.filter(country=country).filter(indicator=indicator):
  #  datapoints.append([[int(d.year.strftime("%Y")),int(d.year.strftime("%m")),int(d.year.strftime("%d"))],d.data])
  #data = json.dumps(datapoints)

  dirpath = 'front-end/media/data/bycountry/'
  filepath = dirpath + country.three_code + '.csv'
  csvfilepath = os.path.join(os.path.dirname(__file__), '../', filepath).replace('\\','/')
  with open(csvfilepath, 'r') as csvfile:
    csvdata = csv.reader(csvfile)
    yearindex = 1
    for row in csvdata:
      if row[0] == "Indicator":
        years = row
      if row[0] == indicator:
        for d in row[1:]:
          if d:
            datapoints.append([[int(years[yearindex]),1,1],float(d)])
          yearindex += 1
    data = json.dumps(datapoints)
  return HttpResponse(data)

def indicators(request):
  headers = ['code', 'name', 'category', 'source', 'description']
  csvfilepath = os.path.join(os.path.dirname(__file__), '../', 'front-end/media/data/indicators.csv').replace('\\','/')
  with open(csvfilepath, 'r') as csvfile:
    csvdata = csv.DictReader(csvfile, headers)
    data = [row for row in csvdata]
  data = json.dumps(data)
  return HttpResponse(data)

def indicator_lookup(request, indicator):
  indicator = Indicator.objects.get(name=indicator)
  data = {
    'name':indicator.name,
    'code':indicator.code,
    'description':indicator.description,
    'category':indicator.category.name,
    'source':indicator.source
  }
  data = json.dumps(data)
  return HttpResponse(data)

def events(request, country):
  try:
    limit = int(request.GET['limit'])
  except:
    limit = None
  try:
    start = int(request.GET['start'])
  except:
    start = 0
  country = Country.objects.get(slug=country)
  filepath = 'front-end/media/data/events/'+country.three_code+'.csv'
  csvfilepath = os.path.join(os.path.dirname(__file__), '../', filepath).replace('\\','/')
  headings = ['date', 'text', 'tags']

  with open(csvfilepath, 'r') as csvfile:
    data = []
    stuff = [r for r in csv.reader(csvfile)]
    numrows = len(stuff)
    if limit:
      length = min(limit, numrows - start)
    else:
      length = numrows - start
    end = start + length
    for i in range(start, end):
      d = stuff[i]
      try:
        #if int(d[0][:4]) > 1959: # get rid of this later
        date = [int(d[0][:4]),int(d[0][5:7]),int(d[0][8:10])] #splitting from "1471-01-15"
        data.append([date,d[1],ast.literal_eval(d[2])])
      except:
        logging.warning("%s %s " % (d[0], d[1]))
        continue
  #data = json.dumps(data)
  metadata = {
    'start':start,
    'end':end,
    'limit':limit,
    'length':length,
    'remaining': numrows - end,
    'total':numrows
    }
  info = {'metadata':metadata, 'data':data}
  info = json.dumps(info)
  return HttpResponse(info)

def country_convert(csvfile):
  csvfilepath = os.path.join(os.path.dirname(__file__), '../', csvfile).replace('\\','/')
  with open(csvfilepath, 'r') as csvfile:
    data = csv.reader(csvfile)
    for row in data:
      new_country = Country(
        three_code = row[0],
        two_code = row[1],
        short_name = row[2],
        long_name = row[3],
        slug = row[4]
        )
      new_country.save()
  return

def indicator_convert(csvfile):
  csvfilepath = os.path.join(os.path.dirname(__file__), '../', csvfile).replace('\\','/')
  with open(csvfilepath, 'r') as csvfile:
    data = csv.reader(csvfile)
    for row in data:
      categories = [c.name for c in Category.objects.all()]
      category = row[2]
      if not category in categories:
        new_category = Category(
          name = category,
          )
        new_category.save()
      new_indicator = Indicator(
        code = row[0],
        name = row[1],
        category = Category.objects.get(name=category),
        description = row[4]
        )
      new_indicator.save()
  return

def datapoint_convert(csvfile, indicator):
  yearscsv = 'front-end/media/data/years.csv'
  yearfile = open(os.path.join(os.path.dirname(__file__), '../', yearscsv).replace('\\','/'), 'r')
  years = [datetime.datetime.strptime(r[0], "%Y") for r in csv.reader(yearfile)]
  yearfile.close()
  indicator = Indicator.objects.get(code=indicator)

  csvfilepath = os.path.join(os.path.dirname(__file__), '../', csvfile).replace('\\','/')

  with open(csvfilepath, 'r') as csvfile:
    data = csv.reader(csvfile)
    for row in data:
      if row[0] == "Country Code":
        continue
      country = Country.objects.get(three_code=row[0])
      yearindex = 0
      for point in row[1:]:
        if point:
          new_datapoint = Datapoint(
            year = years[yearindex],
            country = country,
            indicator = indicator,
            data = float(point)
          )
          new_datapoint.save()
        yearindex += 1
  return

def event_convert(csvfile, country):
  country = Country.objects.get(three_code=str(country).upper())
  csvfilepath = os.path.join(os.path.dirname(__file__), '../', csvfile).replace('\\','/')
  with open(csvfilepath, 'r') as csvfile:
    data = csv.reader(csvfile)
    for row in data:
      new_event = Event(
        country = country,
        text = row[1],
        citation = "http://timelinesdb.com/",
        date = datetime.datetime.strptime(row[0], "%Y-%m-%d")
        )
      new_event.save()

def datadump(request, datatype):
  if re.match(r'^\w{3}$', datatype):
    country = datatype
    dirpath = 'front-end/media/data/events/'
    filepath = dirpath + country + '.csv'
    event_convert(filepath, country)
    return HttpResponse('ok')

  if datatype == 'countries':
    country_convert('front-end/media/data/countries.csv')
    return HttpResponse('ok')

  if datatype == 'indicators':
    indicator_convert('front-end/media/data/indicators.csv')
    return HttpResponse('ok')

  else:
    indicator = datatype
    dirpath = 'front-end/media/data/datapoints/'
    filepath = dirpath + indicator[0:2] + "/" + indicator + '.csv'
    datapoint_convert(filepath, indicator)
    return HttpResponse('ok')

def compare(request):
  gv = globvars
  context = {
    'thispage':'Home'
      }
  context = dict(context, **gv.context) #combines the 'local' context with the 'global' context
  return render_to_response('compare.html', context)
